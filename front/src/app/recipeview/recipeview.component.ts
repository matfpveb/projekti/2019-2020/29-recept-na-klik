import { Component, OnInit, Input } from '@angular/core';
import { RecipeService } from '../services/recipe/recipe.service';
import { RecipeModel } from '../services/recipe/recipe.model';
import { Observable } from 'rxjs';
import { UserService } from '../services/user/user.service';
import { User } from '../services/user/user.model';
import { map } from 'rxjs/operators';
import { DomSanitizer } from '@angular/platform-browser';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-recipeview',
  templateUrl: './recipeview.component.html',
  styleUrls: ['./recipeview.component.css']
})
export class RecipeviewComponent implements OnInit {
  @Input()
  recipeId: string;

  @Input()
  userId: string;

  public recipe: RecipeModel;
  public user : User;
  public imageContent;

  constructor(
    private recipeService: RecipeService, 
    private userService: UserService, 
    private domS : DomSanitizer,
    public loginService: LoginService) { 
  }

  ngOnInit(): void {
    this.getRecipe();
    this.getUser();
  }

  private getRecipe() {
    this.recipeService.getRecipeById(this.recipeId).subscribe((recipe) => {
      this.recipe = recipe;
      let TYPED_ARRAY = new Uint8Array(recipe.photo['data']['data']);
      const STRING_CHAR = String.fromCharCode.apply(null, TYPED_ARRAY);
      let base64String = btoa(STRING_CHAR);
      this.imageContent = this.domS.bypassSecurityTrustUrl('data:image/jpg;base64,' + base64String);
  });
}

  private getUser() {
    this.userService.getUserById(this.userId).subscribe((user) => this.user = user);
  }
}
