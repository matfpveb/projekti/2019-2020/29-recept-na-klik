import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { FindRecipesComponent } from './find-recipes/find-recipes.component';
import { AddRecipesComponent } from './add-recipes/add-recipes.component';
import { RecipeComponent } from './recipe/recipe.component';


const routes: Routes = [
{path: '', component:HomeComponent},
{path: 'user', children: [
  { path: ':userId', component: ProfileComponent },
  { path: ':userId/addRecipes', component: AddRecipesComponent }
]},
{path: 'login', component:LoginComponent},
{path: 'registration' , component:RegistrationComponent},
{path: 'findRecipes', component: FindRecipesComponent},
{path: 'recipe/:recipeId', component: RecipeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload', anchorScrolling: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
