import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../services/user/user.service';
import { User } from '../services/user/user.model';
import { Router } from '@angular/router';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public LogInForm: FormGroup;
  public users: User[];
  public user: User;

  constructor(
    private formBuilder: FormBuilder, 
    private userService: UserService, 
    private router: Router,
    private loginService: LoginService) {
    this.LogInForm = formBuilder.group({      
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
}

  ngOnInit(): void {
    this.userService.refreshUsers("").subscribe((users) => this.users = users)
  }

  public submitLogInForm(formValue: any){
    let ifExists: boolean;
    if (!this.LogInForm.valid) {
      window.alert('Podaci nisu uneti kako treba!');
      return;
    }
    this.users.forEach(x => {
      if (x.username == this.username.value && x.password == this.password.value) {
        ifExists = true;
        this.user = x;
      }
    });

    if (ifExists) {
      this.loginService.logIn(this.user);
      this.router.navigate(['/']);
    } else {
      window.alert('Korisnik ne postoji! Proverite unete podatke!');
      this.LogInForm.reset();
      return;
    }
  }

  public get username(){
    return this.LogInForm.get('username');
  }
  public get password(){
    return this.LogInForm.get('password');
  }

}
