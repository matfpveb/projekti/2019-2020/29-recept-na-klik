const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true
    },
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    favorites: {
        type: [String]
    },
    createdAt: {
        type: Date
    },
    updates: {
        type: [Map],
        of: String
    },
    photo: { 
        data: Buffer, 
        contentType: String
    },
}, {versionKey: false});

module.exports = mongoose.model("User", userSchema);