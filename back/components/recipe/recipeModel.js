const mongoose = require('mongoose');

const recipeSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    title: {
        type: String,
        required: true
    },
    difficulty: {
        type: Number,
        required: true
    },
    preptime: { 
        type: Number,
        required: true
    },
    course: { 
        type: String,
        required: true
    },
    category: { 
        type: [String],
        required: true
    },
    createdAt: { 
        type: String
    },
    updates: { 
        type: [Map],
        of: String
    },
    instructions: { 
        type: [String],
        required: true
    },
    serving: { 
        type: String,
    },
    photo: { 
        data: Buffer, 
        contentType: String
    },
    units: { 
        type: Number,
        required: true
    },
    ingredients: { 
        type: [String],
        required: true
    },
    scores: { 
        type: Map
    },
    comments: { 
        type: [Map],
        of: String
    }
}, {versionKey: false});

module.exports = mongoose.model("Recipe", recipeSchema);