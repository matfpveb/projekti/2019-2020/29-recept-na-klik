const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const indexAPIRoutes = require('./components/index/indexAPI');
const recipesRoutes = require('./components/recipe/recipeAPI');
const userRoutes = require('./components/users/userApi');

const app = express();

mongoose.connect('mongodb://127.0.0.1:27017/receptNaKlik', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
});

app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);

app.use(bodyParser.json({}));

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Content-Type');

  if (req.method === 'OPTIONS') {
    res.header(
      'Access-Control-Allow-Methods',
      'OPTIONS, GET, POST, PATCH, DELETE, PUT'
    );

    return res.status(200).json({});
  }

  next();
});

app.use('/', indexAPIRoutes);
app.use('/recipe', recipesRoutes);
app.use('/user', userRoutes);

app.use(function (req, res, next) {
  const error = new Error('Request is not supported by server');
  error.status = 405;

  next(error);
});

app.use(function (error, req, res, next) {
    const statusCode = error.status || 500;
  res.status(statusCode).json({
    error: {
      message: error.message,
      status: statusCode,
      stack: error.stack
    },
  });
});

module.exports = app;